﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

public class ScriptReadList : MonoBehaviour
{

    public static ListDef myList = new ListDef();
    public static ListUnitTypes lUnitsDict = new ListUnitTypes();

  
    public static void ReadList(string filePath)
    {
        //Reads json into string
        string json = System.IO.File.ReadAllText(filePath);
        //Deserialize json into ListDef 
        myList = JsonConvert.DeserializeObject<ListDef>(json);
        
        myList.unitsL = new List<ListUnitTypes>();//List of Dictionaries Key=Unittype value=units
        lUnitsDict.listUnitTypes = new Dictionary<string, List<ListUnits>>();//Dictionary key=unittype, value=units
        List<ListUnits> zdl = new List<ListUnits>();

         
        foreach (ReadJToken a in myList.units)
        {
            foreach (KeyValuePair<string, JToken> b in a.dictJToken)
            {
                IList<ListUnits> c = JsonConvert.DeserializeObject<IList<ListUnits>>(b.Value.ToString());
                zdl = c as List<ListUnits>;
                lUnitsDict.listUnitTypes.Add(b.Key, zdl);


            }
            myList.unitsL.Add(lUnitsDict);
        }


    }

    
}
