﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static ScriptStart;
using static ScriptReadList;
using static ScriptReadArmy;
using static ButtonScriptListEdit;
using static UpdateScriptMessage;

public class UpdateScriptListEdit : MonoBehaviour
{
    public GameObject StartScriptObject;
    public GameObject MessagePannel;

    public void UpdateListEdit()
    {
        UpdateScriptMessage u1 = MessagePannel.GetComponent<UpdateScriptMessage>();
        Color col = new Color(0.4f, 0.0f, 0.0f, 1.0f);

        if (transform.GetChild(3).GetComponent<InputField>().text != null)//input
        {

            foreach (List<ListUnits> lb in lUnitsDict.listUnitTypes.Values)//Units from list
            {
                foreach (List<Units> ab in aUnitsDict.armyUnitTypes.Values)//units from army
                {
                    foreach (ListUnits lu in lb)
                    {
                        foreach (Units u in ab)
                        {
                            if (lu.name == u.name)
                            {
                                if (transform.GetChild(0).transform.GetComponent<Text>().text == u.name)//name of unit
                                {
                                    
                                    //check if input is correct
                                    if (Int32.Parse(transform.GetChild(3).GetComponent<InputField>().text) < u.minCount)
                                    {

                                        transform.GetChild(3).GetComponent<InputField>().text = u.minCount.ToString();
                                        MessagePannel.transform.GetComponent<Text>().text = string.Format("min=" + u.minCount);
                                        MessagePannel.transform.GetComponent<Text>().color = col;
                                        u1.FadeoutMessage(MessagePannel, 2.0f);

                                    }
                                    if (u.maxCount > 0)
                                    {
                                        if (Int32.Parse(transform.GetChild(3).GetComponent<InputField>().text) > u.maxCount)
                                        {

                                            transform.GetChild(3).GetComponent<InputField>().text = u.maxCount.ToString();
                                            MessagePannel.transform.GetComponent<Text>().text = string.Format("max=" + u.maxCount);
                                            col = MessagePannel.transform.GetComponent<Text>().color;
                                            col.a = 1.0f;
                                            MessagePannel.transform.GetComponent<Text>().color = col;
                                            u1.FadeoutMessage(MessagePannel, 2.0f);
                                        }


                                    }
                                    //set new Value for total cost
                                    int o = u.cost * Int32.Parse(transform.GetChild(3).GetComponent<InputField>().text);
                                    transform.GetChild(1).GetComponent<Text>().text = o.ToString();
                                    lu.count = Int32.Parse(transform.GetChild(3).GetComponent<InputField>().text);

                                }
                            }
                        }
                    }
                }
            }
        }
        else//if no value was set: use minCount 
        {
            foreach (List<ListUnits> lb in lUnitsDict.listUnitTypes.Values)//Units from list
            {
                foreach (List<Units> ab in aUnitsDict.armyUnitTypes.Values)//units from army
                {
                    foreach (ListUnits lu in lb)
                    {
                        foreach (Units u in ab)
                        {
                            if (lu.name == u.name)
                            {
                                if (transform.GetChild(0).transform.GetComponent<Text>().text == u.name)
                                {
                                    //set new Value for total cost
                                    int o = u.cost * Int32.Parse(transform.GetChild(3).transform.GetChild(0).transform.GetComponent<Text>().text);
                                    transform.GetChild(1).GetComponent<Text>().text = o.ToString();
                                    lu.count = Int32.Parse(transform.GetChild(3).GetComponent<InputField>().text);

                                }
                            }
                        }
                    }
                }
            }
        }

        ScriptStart s1 = StartScriptObject.GetComponent<ScriptStart>();
        Transform scrollerContent = s1.scrollerContent;
        GameObject ScrollerContentGA = s1.ScrollerContentGA;


        percentCount.Clear();
        percentCount.Add("Total", 0);
        foreach (KeyValuePair<string, List<ListUnits>> la in lUnitsDict.listUnitTypes)
        {
            foreach (KeyValuePair<string, List<Units>> aa in aUnitsDict.armyUnitTypes)
            {
                if (la.Key == aa.Key)
                {
                    percentCount.Add(la.Key, 0);
                    foreach (ListUnits lb in la.Value)
                    {
                        foreach (Units ab in aa.Value)
                        {
                            if (lb.name == ab.name)
                            {
                                int points;//total cost of a unit

                                if (lb.count != 0)
                                {
                                    points = lb.count * ab.cost;

                                }
                                else
                                {
                                    points = ab.cost;
                                }
                                percentCount["Total"] += points;
                                percentCount[la.Key] += points;

                            }
                        }
                    }
                }
            }
        }

        int tpoints = 0;
        for (int i = 1; i < scrollerContent.childCount; i++)
        {

            Debug.Log("Children:" + scrollerContent.childCount);
            foreach (KeyValuePair<string, int> p in percentCount)
            {
                if (ScrollerContentGA.transform.GetChild(i).transform.GetChild(0).transform.GetComponent<Text>().text == p.Key)
                {
                    tpoints += p.Value;
                    int t = 0;
                    if (percentCount["Total"] != 0)
                    {
                        t = (p.Value * 100) / percentCount["Total"];
                    }

                    ScrollerContentGA.transform.GetChild(i).transform.GetChild(1).transform.GetComponent<Text>().text = string.Format(+p.Value + "P , " + t + "%");
                    ScrollerContentGA.transform.GetChild(i).transform.GetChild(1).transform.GetComponent<Text>().fontStyle = FontStyle.Bold;
                }


            }

        }



        ScrollerContentGA.transform.GetChild(1).transform.GetChild(1).transform.GetComponent<Text>().text = string.Format(tpoints + "P"); ;



    }
}


        


                        


