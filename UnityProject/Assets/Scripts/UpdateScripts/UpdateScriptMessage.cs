﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateScriptMessage : MonoBehaviour
{
    

    // Update is called once per frame
    public void FadeoutMessage(GameObject pannel, float timeToFade)
    {
 
        StartCoroutine(FadeOutRoutine(pannel, timeToFade));

    }

    private IEnumerator FadeOutRoutine(GameObject pannel, float timeToFade)
    {
        Text text = pannel.transform.GetComponent<Text>();
        Color originalColor = text.color;
        for (float t = 0.01f; t < timeToFade; t += Time.deltaTime)
        {
            text.color = Color.Lerp(originalColor, Color.clear, Mathf.Min(1, t / timeToFade));
            yield return null;
        }
    }



}
