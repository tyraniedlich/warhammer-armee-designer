﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;


//TODO: UpgradeDef und ListUpgrade ergänzen

//Datastructure for system.json
public class SystemDef
{
    public string sysName { get; set; } //Name of system
    public string sysType { get; set; } // Type = System
    public string sysLanguage { get; set; } //Language of system
    public List<UnitDef> sysUnits { get; set; } //List of UnitTypes(Hero, Commander, Elite....)
    public List<UpgradeItemDef> sysUpgradeItems { get; set; }//List of Type names for "up-to-x-points"-items
}

public class UnitDef
{
    public string UnitTypeName { get; set; }//Name of a UnitType
    public List<string> UnitUpgradecategoryNames { get; set; }//List of Upgradecategorie-Names for the UnitType(Equip, Abilitys, Magic, Items...)
}

public class UpgradeItemDef
{
    public string UpgradeItemName { get; set; }//Name of UpgradeType
    public List<string> UpgradeItemcategoryNames { get; set; }//List of item Category-Name
}



//###############################################################################################################################################################################################



//Datastructur for army.json
public class ArmyDef
{
    public string name { get; set; }//Name of Army
    public string type { get; set; } // Txpe = Army
    public string system { get; set; }//Name of System of Army
    public IList<ReadJToken> units { get; set; }//List of Unit-Types including Units in Armee
    public List<ArmyUnitTypes> unitsA { get; set; }
    public IList<ReadJToken> upgradeItems { get; set; }//List of Item Types
    public List<UpgradeItemsList> upgradeItemsA { get; set; }

}


public class ArmyUnitTypes
{
    public Dictionary<string, List<Units>> armyUnitTypes { get; set; }//List of Units
}

public class Units
{
    public string name { get; set; }//Name of Unit
    public string type { get; set; }//Stype of Unit (Solo, Troop)
    public int cost { get; set; }//Point Cost of a Unit
    public int minCount { get; set; }//Minimum Count of Soldiers in a Troop-Unit
    public int maxCount { get; set; }//Maximum Count of Soldiers in a Troop-Unit
    public IList<ReadJToken> upgrades { get; set; }//List of UpgradeTypes
}

public class UpgradeTypesList 
{
    public Dictionary<string, List<upgradeDef>> upgradeTypes{ get; set; }//List of Upgrades für a Unit
}


public class upgradeDef
{
    public string name { get; set; }//Name of Upgrade
    public string type { get; set; }//Style of Upgrade
    public int cost { get; set; }
    //weitere müssen ergänzt werden


}

public class UpgradeItemsList
{
    public IDictionary<string, List<UpgradeItemsTypesList>> upgradeItems { get; set; }//Item Types
}

public class UpgradeItemsTypesList 
{
    public IList<ItemsDef> upgradeItemsTypes{ get; set; }//List of Type Names for "Up-to-x-points"-Items
}

public class ItemsDef //"up-to-x-points"-items
{
    public string name { get; set; } //Name of Item
    public int cost { get; set; }//Cost of Item
}


//#######################################################################################################################################################################################################################


//Datastructure of Lists
public class ListDef
{
    public string name { get; set; }//Name of List
    public string type { get; set; }//Type=List
    public string army { get; set; }//Army of List
    public IList<ReadJToken> units { get; set; }//List of UnitTypes
    public List<ListUnitTypes> unitsL { get; set; }
}

public class ListUnitTypes
{
    public Dictionary<string, List<ListUnits>> listUnitTypes { get; set; }
}

public class ListUnits
{
    public string name { get; set; }//UnitName
    public int count { get; set; }//Count of Soldiers in Unit
    public IList<ReadJToken> upgrades { get; set; }//List of Upgrades
    public List<ListUpgradeTypes> upgradesL { get; set; }
}

public class ListUpgradeTypes
{
    public Dictionary<string, List<ListUpgrade>> listUpgradeTypes { get; set; }//Types of Upgrade
}

public class ListUpgrade
{
    public string name { get; set; }//Name of Upgrade
    public int count { get; set; }
    //public List<string> items { get; set; }
    //public IDictionary<string, JToken> upgrades { get; set; }
    
    //Muss erweitert werden
}



//#############################################################################################################################################################################################################

    //only for converting JToken into Dictionary
public class ReadJToken
{
    [JsonExtensionData]
    public IDictionary<string, JToken> dictJToken;
}

public class OutList
{
    public string name { get; set; }//Name of List
    public string type { get; set; }//Type=List
    public string army { get; set; }//Army of List
    public List<Dictionary<string, List<ListUnits>>> units { get; set; }
}