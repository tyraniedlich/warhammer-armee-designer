﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using static ScriptReadList;

public class ButtonScriptListSave : MonoBehaviour
{



    public void ListSave()
    {
        OutList outlist = new OutList();

        Dictionary<string, List<ListUnits>> dict = new Dictionary<string, List<ListUnits>>();
        outlist.units = new List<Dictionary<string, List<ListUnits>>>();
        outlist.name = myList.name;
        outlist.type = myList.type;
        outlist.army = myList.army;

        foreach(KeyValuePair<string, List<ListUnits>> a in lUnitsDict.listUnitTypes)
        {

            dict.Add(a.Key,a.Value);
        }

        outlist.units.Add(dict);
              
        string output = JsonConvert.SerializeObject(outlist);
        string listName = myList.name;
        string filePath = string.Format(ScriptStart.listsPath + "/" + listName+".json");
        System.IO.File.WriteAllText(filePath, output);


    }



}
