﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static ScriptReadArmy;
using static ScriptReadList;
using static ScriptStart;

public class ButtonScriptAddList : MonoBehaviour
{
    public GameObject StartScriptObject;
    public GameObject TextIntTemplate;
    public GameObject InputField;

    public void AddList()
    {

        myList = new ListDef();
        lUnitsDict = new ListUnitTypes();
        lUnitsDict.listUnitTypes = new Dictionary<string, List<ListUnits>>();

        myList.name = "new List";
        myList.type = "List";
        myList.army = myArmy.name;


        ScriptStart s1 = StartScriptObject.GetComponent<ScriptStart>();
        Transform scrollerContent = s1.scrollerContent;
        GameObject addButton = s1.addButton;
        GameObject headlineSystem = s1.headlineSystem;
        GameObject headlineArmy = s1.headlineArmy;
        GameObject headlineLists = s1.headlineLists;
        GameObject headlineListEdit = s1.headlineListEdit;
        GameObject ScrollerContentGA = s1.ScrollerContentGA;

        ScriptClearChildren.ClearChildren(ScrollerContentGA);


        GameObject insertName = Instantiate(InputField, scrollerContent, false);
        insertName.transform.GetComponent<InputField>().text = myList.name;

        GameObject totalpoints = Instantiate(TextIntTemplate, scrollerContent, false);
        totalpoints.transform.GetChild(0).transform.GetComponent<Text>().text = "Total:";
        totalpoints.transform.GetChild(1).transform.GetComponent<Text>().text = "0P";


        foreach (KeyValuePair<string, List<Units>> aa in aUnitsDict.armyUnitTypes)
        {
            lUnitsDict.listUnitTypes.Add(aa.Key, null);
            lUnitsDict.listUnitTypes[aa.Key] = new List<ListUnits>();

            GameObject textCategories = Instantiate(TextIntTemplate, scrollerContent, false);
            textCategories.transform.GetChild(0).transform.GetComponent<Text>().text = aa.Key;
            textCategories.transform.GetChild(0).transform.GetComponent<Text>().fontStyle = FontStyle.Bold;

            GameObject addButtonUnit = Instantiate(addButton, scrollerContent, false);
            addButtonUnit.transform.GetChild(1).transform.GetComponent<Text>().text = aa.Key;

        }




    }



}
