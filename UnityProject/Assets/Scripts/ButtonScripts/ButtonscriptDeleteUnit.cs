﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static ScriptStart;
using static ScriptReadList;
using static ScriptReadArmy;
using static ButtonScriptListEdit;

public class ButtonscriptDeleteUnit : MonoBehaviour
{

    public GameObject StartScriptObject;
    public GameObject ListEditTemplate;
    public GameObject TextIntTemplate;
    public GameObject HeadlineChooseUnit;
    public GameObject InputField;

    public void DeleteUnit()
    {
        ScriptStart s1 = StartScriptObject.GetComponent<ScriptStart>();
        Transform scrollerContent = s1.scrollerContent;
        GameObject addButton = s1.addButton;
        GameObject headlineSystem = s1.headlineSystem;
        GameObject headlineArmy = s1.headlineArmy;
        GameObject headlineLists = s1.headlineLists;
        GameObject headlineListEdit = s1.headlineListEdit;
        GameObject ScrollerContentGA = s1.ScrollerContentGA;



        int objectIndex = transform.GetSiblingIndex();
        Debug.Log("Sindex:" + objectIndex);
        int categoryIndex=0;
        int dataIndex = 0;
        bool stop = false;
        for (int i = objectIndex; stop != true; i--)
        {
            foreach (KeyValuePair<string, List<ListUnits>> la in lUnitsDict.listUnitTypes)
            {
                if (ScrollerContentGA.transform.GetChild(i).transform.GetChild(0).transform.GetComponent<Text>().text == la.Key)
                {
                    categoryIndex = i;
                    stop = true;
                    Debug.Log("CIndex:" + categoryIndex);

                    dataIndex = objectIndex - categoryIndex - 1;
                    la.Value.RemoveAt(dataIndex);
                }
            }
                
        }

        ScriptClearChildren.ClearChildren(ScrollerContentGA);

        foreach (KeyValuePair<string,List<ListUnits>> la in lUnitsDict.listUnitTypes)
        {
            Debug.Log("Key: " + la.Key);
            foreach( ListUnits b in la.Value)
            {
                Debug.Log("val: " + b.name);
            }


        }

        GameObject insertName = Instantiate(InputField, scrollerContent, false);
        insertName.transform.GetComponent<InputField>().text = myList.name;

        GameObject totalpoints = Instantiate(TextIntTemplate, scrollerContent, false);
        totalpoints.transform.GetChild(0).transform.GetComponent<Text>().text = "Total:";

        percentCount.Clear();
        percentCount.Add("Total", 0);
        foreach (KeyValuePair<string, List<ListUnits>> la in lUnitsDict.listUnitTypes)
        {
            foreach (KeyValuePair<string, List<Units>> aa in aUnitsDict.armyUnitTypes)
            {
                if (la.Key == aa.Key)
                {

                    GameObject textCategories = Instantiate(TextIntTemplate, scrollerContent, false);
                    textCategories.transform.GetChild(0).transform.GetComponent<Text>().text = la.Key;
                    textCategories.transform.GetChild(0).transform.GetComponent<Text>().fontStyle = FontStyle.Bold;

                    percentCount.Add(la.Key, 0);

                    foreach (ListUnits lb in la.Value)
                    {
                        foreach (Units ab in aa.Value)
                        {
                            if (lb.name == ab.name)
                            {
                                //create ListEditTemplate Objects
                                GameObject showUnits = Instantiate(ListEditTemplate, scrollerContent, false);

                                //check unit count from list for first calculation of total cost for a unit
                                int points;//total cost of a unit

                                if (lb.count != 0)
                                {
                                    points = lb.count * ab.cost;

                                }
                                else
                                {
                                    points = ab.cost;
                                }

                                //sets name and total cost
                                showUnits.transform.GetChild(0).transform.GetComponent<Text>().text = lb.name;
                                showUnits.transform.GetChild(1).transform.GetComponent<Text>().text = points.ToString();

                                percentCount["Total"] += points;
                                percentCount[la.Key] += points;

                                //onely a troop has a count of soldiers to set
                                //unneeded parts of template will be deactivated for solo units
                                if (ab.type == "Troop")
                                {
                                    showUnits.transform.GetChild(3).transform.GetChild(0).transform.GetComponent<Text>().text = ab.minCount.ToString();
                                    showUnits.transform.GetChild(3).transform.GetComponent<InputField>().text = lb.count.ToString();
                                }
                                else
                                {
                                    showUnits.transform.GetChild(2).transform.gameObject.SetActive(false);
                                    showUnits.transform.GetChild(3).transform.gameObject.SetActive(false);

                                }
                            }
                        }
                    }

                    GameObject addButtonUnit = Instantiate(addButton, scrollerContent, false);
                    addButtonUnit.transform.GetChild(1).transform.GetComponent<Text>().text = la.Key;

                }
            }
        }



        headlineArmy.SetActive(false);
        headlineSystem.SetActive(false);
        headlineLists.SetActive(false);
        headlineListEdit.SetActive(true);
        HeadlineChooseUnit.SetActive(false);



        int tpoints = 0;
        for (int i = 1; i < scrollerContent.childCount; i++)
        {
            Debug.Log("Children:" + scrollerContent.childCount);
            foreach (KeyValuePair<string, int> p in percentCount)
            {
                if (ScrollerContentGA.transform.GetChild(i).transform.GetChild(0).transform.GetComponent<Text>().text != null)
                {
                    if (ScrollerContentGA.transform.GetChild(i).transform.GetChild(0).transform.GetComponent<Text>().text == p.Key)
                    {
                        tpoints += p.Value;
                        int t = 0;
                        if (percentCount["Total"] != 0)
                        {
                            t = (p.Value * 100) / percentCount["Total"];
                        }
                       
                        ScrollerContentGA.transform.GetChild(i).transform.GetChild(1).transform.GetComponent<Text>().text = string.Format(+p.Value + "P , " + t + "%");
                        ScrollerContentGA.transform.GetChild(i).transform.GetChild(1).transform.GetComponent<Text>().fontStyle = FontStyle.Bold;
                    }
                }


            }

        }

        Debug.Log("tpoints: " + tpoints);
        totalpoints.transform.GetChild(1).transform.GetComponent<Text>().text = string.Format(tpoints + "P"); ;

    }


}
