﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static ScriptStart;
using static ScriptReadArmy;
using static ScriptReadList;
using static ButtonScriptListEdit;

public class ButtonScriptExitListEditor : MonoBehaviour
{

    public GameObject StartScriptObject;
    public GameObject listsButtonpair;
    public GameObject AddListButton;

    public void ExitListEditor()
    {
        // gets needed components from other script
        ScriptStart s1 = StartScriptObject.GetComponent<ScriptStart>();
        Transform scrollerContent = s1.scrollerContent;
        GameObject addButton = s1.addButton;
        GameObject headlineSystem = s1.headlineSystem;
        GameObject headlineArmy = s1.headlineArmy;
        GameObject headlineLists = s1.headlineLists;
        GameObject headlineListEdit = s1.headlineListEdit;
        GameObject ScrollerContentGA = s1.ScrollerContentGA;

        


        //Create list with filenames
        List<string> dirNames = ScriptListSubdirectorys.ListSubfiles(listsPath);

        ScriptCreateButton.CreateButtons(listsPath, listsButtonpair, ScrollerContentGA, addButton, dirNames, 1);

        GameObject Add = Instantiate(AddListButton, scrollerContent, false);

        //Guess thats not cool, but what should i do? 
        myList = new ListDef();
        lUnitsDict = new ListUnitTypes();
        lUnitsDict.listUnitTypes = new Dictionary<string, List<ListUnits>>();
        percentCount.Clear();
        

        headlineLists.SetActive(true);
        headlineArmy.SetActive(false);
        headlineSystem.SetActive(false);
        headlineListEdit.SetActive(false);
    }




}
   

