﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonScriptExit : MonoBehaviour
{
    

    public void Exit()
    {
        //Application.Quit();
        AndroidJavaObject activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
        activity.Call<bool>("moveTaskToBack", true);

    }

}
