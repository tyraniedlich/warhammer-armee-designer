﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using static ScriptStart;

public class ButtonScriptDeleteList : MonoBehaviour
{

    public GameObject TextOfTwinbutton;
    public GameObject ListButton;


    public void DeleteList()
    {

        string listName = TextOfTwinbutton.transform.GetComponent<Text>().text;
        string filepath = string.Format(listsPath + "/" + listName);
        Debug.Log(filepath);
        File.Delete(filepath);

        Destroy(ListButton);


    }



}
