﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static ScriptStart;


public class ButtonScriptLists : MonoBehaviour
{


    public GameObject StartScriptObject;
    public GameObject listsButtonpair;
    public GameObject AddListButton;



    public void ToLists()
    {
        // gets needed components from other script
        ScriptStart s1 = StartScriptObject.GetComponent<ScriptStart>();
        Transform scrollerContent = s1.scrollerContent;
        GameObject addButton = s1.addButton;
        GameObject headlineSystem = s1.headlineSystem;
        GameObject headlineArmy = s1.headlineArmy;
        GameObject headlineLists = s1.headlineLists;
        GameObject headlineListEdit = s1.headlineListEdit;
        GameObject ScrollerContentGA = s1.ScrollerContentGA;

        //Create Path to List Folder
        string armyName = gameObject.transform.GetChild(0).transform.GetComponent<Text>().text;

        if (armyName != "Back")
        {
            armyPath = string.Format(systemPath + "/" + armyName);
            listsPath = string.Format(armyPath+ "/Lists");

        }
        //Reads armyList into myArmy and aUnitsDict
        ScriptReadArmy.ReadArmy(armyPath, armyName);


        //Create list with filenames
        List<string> dirNames = ScriptListSubdirectorys.ListSubfiles(listsPath);
        Debug.Log(dirNames);

        ScriptCreateButton.CreateButtons(listsPath, listsButtonpair, ScrollerContentGA, addButton, dirNames, 1);

        GameObject AddButton = Instantiate(AddListButton, scrollerContent, false);

        headlineLists.SetActive(true);
        headlineArmy.SetActive(false);
        headlineSystem.SetActive(false);
        headlineListEdit.SetActive(false);
    }
    
}
