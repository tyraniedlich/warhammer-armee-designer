﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static ScriptStart;

public class ButtonScriptArmy : MonoBehaviour
{
    

    public GameObject StartScriptObject;
    public GameObject armyButtonpair;

 
    // Start is called before the first frame update
    public void ToArmy()
    {
        
        ScriptStart s1 = StartScriptObject.GetComponent<ScriptStart>();
        Transform scrollerContent = s1.scrollerContent;
        GameObject addButton = s1.addButton;
        GameObject EmptyObject = s1.EmptyObject;
        GameObject headlineSystem = s1.headlineSystem;
        GameObject headlineArmy = s1.headlineArmy;
        GameObject headlineLists = s1.headlineLists;
        GameObject headlineListEdit = s1.headlineListEdit;
        GameObject ScrollerContentGA = s1.ScrollerContentGA;

        //Create Path to System Folder
        string sysName = gameObject.transform.GetChild(0).transform.GetComponent<Text>().text;

        if (sysName!= "Back")
        {
            systemPath = string.Format(dataPath + "/" + sysName);
            Debug.Log("SystemPath: " + systemPath);
        }
        

        //gets list of subfolders
        List<string> dirNames = ScriptListSubdirectorys.ListSubdirectorys(systemPath);

        //creates buttons
        ScriptCreateButton.CreateButtons(systemPath, armyButtonpair, ScrollerContentGA, EmptyObject, dirNames, 1);

        headlineArmy.SetActive(true);
        headlineSystem.SetActive(false);
        headlineLists.SetActive(false);
        headlineListEdit.SetActive(false);
    }

    
}
