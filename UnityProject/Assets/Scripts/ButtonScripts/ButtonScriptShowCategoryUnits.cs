﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static ScriptReadArmy;




public class ButtonScriptShowCategoryUnits : MonoBehaviour
{

    public GameObject ButtonUnitTemplate;
    public GameObject StartScriptObject;



    public void ShowCategoryUnitsUnits()
    {
        ScriptStart s1 = StartScriptObject.GetComponent<ScriptStart>();
        Transform scrollerContent = s1.scrollerContent;
        GameObject headlineSystem = s1.headlineSystem;
        GameObject headlineArmy = s1.headlineArmy;
        GameObject headlineLists = s1.headlineLists;
        GameObject headlineListEdit = s1.headlineListEdit;
        GameObject headlineChooseUnit = s1.headlineChooseUnit;
        GameObject ScrollerContentGA = s1.ScrollerContentGA;


        ScriptClearChildren.ClearChildren(ScrollerContentGA);


        foreach (KeyValuePair<string, List<Units>> aa in aUnitsDict.armyUnitTypes)
        {
            if (aa.Key == transform.GetChild(1).transform.GetComponent<Text>().text)
            {
               foreach (Units ab in aUnitsDict.armyUnitTypes[aa.Key])
                {
                    GameObject menuButton = Instantiate(ButtonUnitTemplate, ScrollerContentGA.transform, false);
                    menuButton.transform.GetChild(0).transform.GetChild(0).transform.GetComponent<Text>().text = ab.name;

                }

            }
        }


        headlineChooseUnit.SetActive(true);
        headlineSystem.SetActive(false);
        headlineArmy.SetActive(false);
        headlineLists.SetActive(false);
        headlineListEdit.SetActive(false);


    }
}
