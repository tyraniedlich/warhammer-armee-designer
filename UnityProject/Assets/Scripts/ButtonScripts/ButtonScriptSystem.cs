﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static ScriptStart;

public class ButtonScriptSystem : MonoBehaviour
{

    public GameObject StartScriptObject;



    public void ToSystem()
    {
        //Gets needed components from other script
        ScriptStart s1 = StartScriptObject.GetComponent<ScriptStart>();
        string dataPath = ScriptStart.dataPath;
        GameObject panelButtonpair = s1.systemButtonpair;
        Transform scrollerContent = s1.scrollerContent;
        GameObject addButton = s1.addButton;
        GameObject headlineSystem = s1.headlineSystem;
        GameObject headlineArmy = s1.headlineArmy;
        GameObject headlineLists = s1.headlineLists;
        GameObject headlineListEdit = s1.headlineListEdit;
        GameObject ScrollerContentGA = s1.ScrollerContentGA;

        //Gets list of subfolders
        List<string> dirNames = ScriptListSubdirectorys.ListSubdirectorys(dataPath);

        //creates buttons
        ScriptCreateButton.CreateButtons(dataPath, panelButtonpair, ScrollerContentGA, addButton, dirNames, 1);

        headlineSystem.SetActive(true);
        headlineArmy.SetActive(false);
        headlineLists.SetActive(false);
        headlineListEdit.SetActive(false);
    }



    
}
