﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;


public class ScriptListSubdirectorys : MonoBehaviour
{
    // To list directorys
    public static List<string> ListSubdirectorys(string dataPath)
    {
        // Gets DirectoryInformation of all subdirectorys in dataPath-directory
        DirectoryInfo dir = new DirectoryInfo(dataPath);
        DirectoryInfo[] dirInfo = dir.GetDirectories();

        //Gets a list of subdirectory-names in directory
        List<string> dirNames = new List<string>();
        foreach (DirectoryInfo f in dirInfo)
        {
            //Debug.Log(f.Name);
            dirNames.Add(f.Name);

        }

        return dirNames;
    }

    // to list files
    public static List<string> ListSubfiles(string dataPath)
    {
        string[] dir = Directory.GetFiles(dataPath,"*.json");
        List<string> dirNames = new List<string>();

        for (int i = 0; i < dir.Length; i++)
        {
            dirNames.Add(Path.GetFileName(dir[i]));
        }    

        return dirNames;
    }

   
}
