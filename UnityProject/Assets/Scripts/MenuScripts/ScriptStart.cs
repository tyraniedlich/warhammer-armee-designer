﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using UnityEngine.UI;



public class ScriptStart : MonoBehaviour
{
    public GameObject systemButtonpair;
    public Transform scrollerContent;
    public GameObject ScrollerContentGA;
    public GameObject addButton;
    public GameObject EmptyObject;
    public GameObject headlineSystem;
    public GameObject headlineArmy;
    public GameObject headlineLists;
    public GameObject headlineListEdit;
    public GameObject headlineChooseUnit;





    public static string dataPath; //Path of "Data"-folder
    public static string systemPath;//Data/System
    public static string armyPath;//Data/System/Army
    public static string listsPath; //Data/System/Army/Lists
    public static string listFilePath;//Data/System/Army/Lists/list.json

    public static int CountNullObjects;

    // Start is called before the first frame update
    void Start()
    {
        


    //get string of "Data"-folder
    dataPath = string.Format(Application.persistentDataPath + "/Data");
        Debug.Log("dataPath: " + dataPath);
        

        //prove if directory exists
        if (!Directory.Exists(dataPath))
        {
            //if not, create it
            Directory.CreateDirectory(dataPath);
        }


        //get string-list with subdirectory-names of dataPath-directory
        List<string> dirNames = ScriptListSubdirectorys.ListSubdirectorys(dataPath);

        //Create Buttons from system-solders
        ScriptCreateButton.CreateButtons(dataPath, systemButtonpair, ScrollerContentGA, EmptyObject, dirNames, 0);

        
    }

   
 



    
}

