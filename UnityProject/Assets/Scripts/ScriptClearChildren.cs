﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static ScriptStart;

public class ScriptClearChildren : MonoBehaviour
{
   
    public static void ClearChildren(GameObject parent)
    {


        //Clears the scrollerContent
        if (parent.transform.childCount != 0)
        {
            foreach (Transform child in parent.transform)
            {
                GameObject.Destroy(child.gameObject);
            }
        }

        CountNullObjects = parent.transform.childCount;


    }
    


}
