﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

public class ScriptReadArmy : MonoBehaviour
{

    public static ArmyDef myArmy = new ArmyDef();
    public static ArmyUnitTypes aUnitsDict = new ArmyUnitTypes();//zt

    public static void ReadArmy(string armyPath, string armyName)
    {
        //Reads json into string
        string filePath = string.Format(armyPath + "/" + armyName + ".json");
        string json = System.IO.File.ReadAllText(filePath);
        
        //Deserialize json into ListDef 
        myArmy = JsonConvert.DeserializeObject<ArmyDef>(json);

        myArmy.unitsA = new List<ArmyUnitTypes>();//List of Dictionaries Key=Unittype value=units
        aUnitsDict.armyUnitTypes = new Dictionary<string, List<Units>>();//Dictionary key=unittype, value=units
        List<Units> zdl = new List<Units>();
        
        foreach (ReadJToken a in myArmy.units)
        {
            
            foreach (KeyValuePair<string, JToken> b in a.dictJToken)
            {
                IList<Units> c = JsonConvert.DeserializeObject<IList<Units>>(b.Value.ToString());//c ist zwischenspeicher
                zdl = c as List<Units>; 
                aUnitsDict.armyUnitTypes.Add(b.Key, zdl);

            }
            myArmy.unitsA.Add(aUnitsDict);
        }
       

    }

}
